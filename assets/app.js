/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

/** Import components as tag HTML **/
// Admin components
Vue.component('home-admin', require('./components/admin/HomeAdmin.vue').default);
Vue.component('about-manager', require('./components/admin/AboutManager.vue').default);

Vue.component('skill-add', require('./components/skills/AddSkill.vue').default);
Vue.component('skill-remove', require('./components/skills/DestroySkill.vue').default);
Vue.component('skills', require('./components/skills/Skills.vue').default);
Vue.component('skill', require('./components/skills/Skill.vue').default);

Vue.component('prestation-add', require('./components/prestation/AddPrestation.vue').default);
Vue.component('prestation-remove', require('./components/prestation/DestroyPrestation.vue').default);
Vue.component('prestations', require('./components/prestation/Prestations.vue').default);
Vue.component('prestation', require('./components/prestation/Prestation.vue').default);

Vue.component('gallery-add', require('./components/gallery/AddGallery.vue').default);
Vue.component('gallery-remove', require('./components/gallery/DestroyGallery.vue').default);
Vue.component('galleries', require('./components/gallery/Galleries.vue').default);
Vue.component('gallery', require('./components/gallery/Gallery.vue').default);

// Services
Vue.component('img-uploader', require('./components/services/ImageUploader.vue').default);

// Front components
Vue.component('menu-nav', require('./components/Menu.vue').default);
Vue.component('about', require('./components/About.vue').default);
Vue.component('about-description', require('./components/AboutDescription.vue').default);
Vue.component('about-image', require('./components/AboutImage.vue').default);
Vue.component('contact-form', require('./components/ContactForm.vue').default);

/*-- Import component as endpoint routes --*/
import Vue from 'vue';
import Home from './components/Home'


/** vue-toastr (for notifications) **/
import Toastr from 'vue-toastr';
Vue.use(Toastr);

/***  Routing  ***/
import VueRouter from 'vue-router';
Vue.use(VueRouter);

/** Axios (request manager GET, POST, ...) **/
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);

/*-- Setting Routes components --*/
const routes = [
  {
    path: '/',
    component: Home
  }
];

const router = new VueRouter({routes}); // assign routes conf

new Vue({
    el: '#app',
    router: router
  });